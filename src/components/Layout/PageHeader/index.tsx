import { Dropdown, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { useState } from 'react';

import styles from './styles.module.scss';
import useToggleSideNav from 'utils/hooks/useToggleSideNav';
import useProfile from 'utils/hooks/useProfile';
import { logoutWithNotApi } from 'utils/helper/authentication';
import { LanguageType, LocalStorageCode } from 'constants/enum';

export default function PageHeader() {
  const [t] = useTranslation();

  const { profile } = useProfile();
  const { toggleSideNav } = useToggleSideNav();
  const [currentLanguageCode, setCurrentLanguageCode] = useState(
    localStorage.getItem(LocalStorageCode.I18)
  );

  const items = [
    {
      key: '1',
      label: (
        <div
          onClick={() => {
            logoutWithNotApi();
          }}
        >
          {t('common.logout')}
        </div>
      ),
    },
  ];
  const handleSelectLanguageChange = (value: any) => {
    setCurrentLanguageCode(value);
    i18next.changeLanguage(value);
  };

  return (
    <div className={styles.headerWrapper}>
      <svg
        height="32"
        width="32"
        style={{ cursor: 'pointer' }}
        onClick={toggleSideNav}
      >
        <path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z" />
      </svg>
      <div className={styles.actions}>
        <Select
          className={styles.languages}
          onChange={handleSelectLanguageChange}
          value={currentLanguageCode}
        >
          <Select.Option value={LanguageType.JA}>
            {t('languages.japanese')}
          </Select.Option>
          <Select.Option value={LanguageType.EN}>
            {t('languages.english')}
          </Select.Option>
          <Select.Option value={LanguageType.VI}>
            {t('languages.vietnamese')}
          </Select.Option>
          <Select.Option value={LanguageType.KEY}>
            {t('languages.showKey')}
          </Select.Option>
        </Select>
        <div className={styles.menuWrapper}>
          <div className={styles.menuItem}>
            <Dropdown menu={{ items }} trigger={['click']}>
              <div>
                <span>
                  {t('common.welcomeUser', {
                    name: profile?.fullName,
                  })}
                </span>
                &nbsp;
              </div>
            </Dropdown>
          </div>
        </div>
      </div>
    </div>
  );
}
