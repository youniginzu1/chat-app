import { sendDelete, sendGet, sendPost } from './axios';

export const getProfile = () => sendGet('/user/profile');
export const postLogin = (payload: any) => sendPost('/auth/login', payload);
export const deleteLogout = () => sendDelete('/auth/logout');
