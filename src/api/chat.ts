import { sendGet } from './axios';

export const getConversations = () => sendGet('/room/list');
export const getConversationDetail = (roomId?: string | number) =>
  sendGet(`/room/${roomId}`);
export const getChatHistories = (params: any, roomId?: string | number) =>
  sendGet(`/room/${roomId}/history`, params);
