import { Navigate, Outlet } from 'react-router-dom';
import { Suspense } from 'react';

import styles from './styles.module.scss';
import storage from 'utils/helper/storage';
import useProfile from 'utils/hooks/useProfile';
import SideNav from 'components/Layout/SideNav';
import PageHeader from 'components/Layout/PageHeader';

export default function AuthWrapper() {
  const isAuthenticated = !!storage.getToken();
  const { profile } = useProfile(isAuthenticated);

  if (!isAuthenticated) return <Navigate to="/login" />;
  if (!profile) return null;

  return (
    <div className={styles.pageWrapper}>
      <SideNav />
      <div className={styles.mainWrapper}>
        <PageHeader />
        <div className={styles.pageContent}>
          <Suspense fallback={null}>
            <Outlet />
          </Suspense>
        </div>
      </div>
    </div>
  );
}
