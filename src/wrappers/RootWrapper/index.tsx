import { Route, Routes } from 'react-router-dom';
import { lazy } from 'react';

import AuthWrapper from 'wrappers/AuthWrapper';
import ChatWrapper from 'wrappers/ChatWrapper';

const Home = lazy(() => import('pages/Base/Home'));
const Chat = lazy(() => import('pages/Chat'));
const Task = lazy(() => import('pages/Base/Task'));
const Login = lazy(() => import('pages/Auth/Login'));
const SignUp = lazy(() => import('pages/Auth/SignUp'));
const NotFound = lazy(() => import('pages/Response/NotFound'));
const AccessDenied = lazy(() => import('pages/Response/AccessDenied'));

export default function RootWrapper() {
  return (
    <Routes>
      <Route path="/" element={<AuthWrapper />}>
        <Route path="/" element={<Home />} />
        <Route path="/task" element={<Task />} />
      </Route>
      <Route path="/" element={<ChatWrapper />}>
        <Route path="/chat" element={<Chat />} />
        <Route path="/chat/:id" element={<Chat />} />
      </Route>
      <Route path="/login" element={<Login />} />
      <Route path="/sign-up" element={<SignUp />} />
      <Route path="access-denied" element={<AccessDenied />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}
