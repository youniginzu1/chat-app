import { io } from 'socket.io-client';

import configs from 'constants/config';

const socket = io(configs.API_DOMAIN);

export default socket;
