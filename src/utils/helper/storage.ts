import Cookies from 'js-cookie';

import { TokenType } from 'constants/enum';

const EXPIRES = 1;

const setItem = (key: string, value: string) => {
  Cookies.set(key, value, {
    expires: EXPIRES,
    path: '/',
  });
};

const getItem = (key: string) => {
  return Cookies.get(key);
};

const clearItem = (key: string) => {
  Cookies.remove(key, { path: '/' });
};

const setToken = (value: string) => {
  setItem(TokenType.TOKEN, value);
};

const getToken = () => {
  return getItem(TokenType.TOKEN);
};

const clearToken = () => {
  clearItem(TokenType.TOKEN);
};

const setRefreshToken = (value: string) => {
  setItem(TokenType.REFRESH_TOKEN, value);
};

const getRefreshToken = () => {
  return getItem(TokenType.REFRESH_TOKEN);
};

const clearRefreshToken = () => {
  clearItem(TokenType.REFRESH_TOKEN);
};

const storage = {
  setItem,
  getItem,
  clearItem,
  setToken,
  getToken,
  clearToken,
  setRefreshToken,
  getRefreshToken,
  clearRefreshToken,
};
export default storage;
