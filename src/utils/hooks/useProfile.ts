import { useQuery } from '@tanstack/react-query';

import { getProfile } from 'api/auth';
import { QueryKey } from 'constants/enum';
import { IProfile } from 'constants/interface';

export default function useProfile(enabled = false) {
  const {
    data: profile,
    refetch: refetchProfile,
    isLoading: loadingProfile,
    isFetching: fetchingProfile,
  } = useQuery<IProfile>([QueryKey.PROFILE], getProfile, {
    enabled,
    keepPreviousData: true,
  });
  return { profile, refetchProfile, loadingProfile, fetchingProfile };
}
