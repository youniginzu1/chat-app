import { Input, Button, Form } from 'antd';
import { useTranslation } from 'react-i18next';
import { Link, Navigate } from 'react-router-dom';

import styles from './styles.module.scss';
import storage from 'utils/helper/storage';
import { useLogin } from 'utils/helper/authentication';
import { RuleForm } from 'constants/ruleForm';
import images from 'assets';

export default function Login() {
  const [t] = useTranslation();

  const { login, loadingLogin } = useLogin();

  const handleSubmit = (payload: any) => {
    if (!loadingLogin) {
      login(payload);
    }
  };

  const isAuthenticated = !!storage.getToken();
  if (isAuthenticated) return <Navigate to="/" />;

  return (
    <div className={styles.login}>
      <div className={styles.leaves}>
        <div className={styles.set}>
          <div>
            <img src={images.leaf01} alt="" />
          </div>
          <div>
            <img src={images.leaf02} alt="" />
          </div>
          <div>
            <img src={images.leaf03} alt="" />
          </div>
          <div>
            <img src={images.leaf04} alt="" />
          </div>
        </div>
      </div>
      <img src={images.bg} className={styles.bg} alt="" />
      <img src={images.girl} className={styles.girl} alt="" />
      <img src={images.trees} className={styles.trees} alt="" />
      <Form className={styles.form} onFinish={handleSubmit}>
        <h2>{t('common.signIn')}</h2>
        <Form.Item
          name="email"
          rules={RuleForm.username()}
          className={styles.email}
        >
          <Input placeholder={t('placeholder.email')} />
        </Form.Item>
        <Form.Item
          name="password"
          rules={RuleForm.password()}
          className={styles.password}
        >
          <Input.Password placeholder={t('placeholder.password')} />
        </Form.Item>
        <div className={styles.submit}>
          <Button htmlType="submit">{t('common.login')}</Button>
        </div>
        <div className={styles.group}>
          <Link to="#">{t('login.forgotPassword')}</Link>
          <Link to="/sign-up">{t('common.signUp')}</Link>
        </div>
      </Form>
    </div>
  );
}
