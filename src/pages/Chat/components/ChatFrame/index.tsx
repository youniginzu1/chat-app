import { useEffect, useState, useMemo, useContext } from 'react';
import { MdCall, MdSearch, MdOutlineMoreVert } from 'react-icons/md';
import { BsEmojiSmile } from 'react-icons/bs';
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { Input, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Loading3QuartersOutlined } from '@ant-design/icons';

import styles from './styles.module.scss';
import { FormatTime, QueryKey } from 'constants/enum';
import { getChatHistories, getConversationDetail } from 'api/chat';
import { IChatContext, IChatDetailItem, IFilter } from 'constants/interface';
import ChatDetailItem from '../ChatDetailItem';
import { convertTimeToLocal, trimText } from 'utils/helper';
import socket from 'utils/helper/socket';
import useProfile from 'utils/hooks/useProfile';
import useFilter from 'utils/hooks/useFilter';
import { ChatContext } from 'pages/Chat';

interface IState {
  allMessage: IChatDetailItem[];
  hasMore: boolean;
}

const defaultState: IState = {
  allMessage: [],
  hasMore: false,
};
const defaultFilter: IFilter = {
  pageIndex: 1,
  pageSize: 20,
};

export default function ChatFrame() {
  const params = useParams();

  const { receiveMes } = useContext(ChatContext) as IChatContext;
  const [message, setMessage] = useState('');
  const { filter, setFilter, resetFilter } = useFilter(defaultFilter);
  const [state, setState] = useState<IState>(defaultState);

  const isResetState = useMemo(() => {
    const firstMessage = state.allMessage?.[0];
    if (firstMessage && `${firstMessage?.roomId}` !== params?.id) {
      const chatHistory = document.querySelector('#chatHistory');
      if (Number(chatHistory?.scrollTop) !== 0) {
        chatHistory?.scrollTo(0, 0);
      }
      resetFilter();
    }
    return firstMessage && `${firstMessage?.roomId}` !== params?.id;
  }, [params?.id, state]);

  const { profile } = useProfile();
  const { data: conversationDetail } = useQuery(
    [QueryKey.CONVERSATION_DETAIL, params?.id],
    () => getConversationDetail(params?.id),
    {
      enabled: !!params?.id,
    }
  );
  const { data: chatHistories, isFetching: fetching } = useQuery(
    [QueryKey.CHAT_HISTORIES, params?.id, filter],
    () => getChatHistories(filter, params?.id),
    {
      enabled: !!params?.id,
    }
  );

  useEffect(() => {
    if (chatHistories && !fetching) {
      if (isResetState) {
        setState({
          ...defaultState,
          allMessage: [...chatHistories?.data],
          hasMore: chatHistories?.data?.length === filter.pageSize,
        });
      } else {
        setState({
          ...state,
          allMessage: [...state.allMessage, ...chatHistories?.data],
          hasMore: chatHistories?.data?.length === filter.pageSize,
        });
      }
    }
  }, [fetching, chatHistories]);
  useEffect(() => {
    if (receiveMes) {
      setState({
        ...state,
        allMessage: [receiveMes, ...state.allMessage],
      });
    }
  }, [receiveMes]);

  const loadMoreMessage = () => {
    if (fetching) return;
    setFilter({ ...filter, pageIndex: (filter.pageIndex || 1) + 1 });
  };
  const handleKeydown = (e: any) => {
    if (e.key === 'Enter') {
      const mes = trimText(message);
      if (mes) {
        socket.emit('createMessage', {
          senderId: profile?.id,
          senderType: 1,
          content: mes,
          roomId: conversationDetail?.id,
        });
      }
      setMessage('');
    }
  };
  const handleMessageChange = (e: any) => {
    setMessage(e.target?.value);
  };

  return (
    <div className={styles.chatFrame}>
      <div className={styles.header}>
        <div className={styles.headerAvatar}>
          <img src={conversationDetail?.avatar} alt="" />
        </div>
        <div className={styles.headerName}>
          <span>{conversationDetail?.name}</span>
          <span>
            {convertTimeToLocal(conversationDetail?.createdAt, FormatTime.TIME)}
          </span>
        </div>
        <div className={styles.headerIcon}>
          <MdSearch className={styles.icon}></MdSearch>
          <MdCall className={styles.icon}></MdCall>
          <MdOutlineMoreVert className={styles.icon}></MdOutlineMoreVert>
        </div>
      </div>
      <div className={styles.content}>
        <div className={styles.chatHistory} id="chatHistory">
          <InfiniteScroll
            dataLength={state.allMessage?.length || 0}
            next={loadMoreMessage}
            hasMore={state.hasMore}
            inverse={true}
            loader={
              <Spin
                indicator={
                  <Loading3QuartersOutlined style={{ fontSize: 24 }} spin />
                }
              />
            }
            scrollableTarget="chatHistory"
            className={styles.scrollContainer}
          >
            {state.allMessage?.map((item: IChatDetailItem) => (
              <ChatDetailItem key={item?._id} item={item} />
            ))}
          </InfiniteScroll>
        </div>
        <div className={styles.chatInput}>
          <BsEmojiSmile className={styles.emoji}></BsEmojiSmile>
          <Input
            placeholder="Message"
            value={message}
            onChange={handleMessageChange}
            onKeyDown={handleKeydown}
          />
        </div>
      </div>
    </div>
  );
}
