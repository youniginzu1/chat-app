import classNames from 'classnames';

import { IChatDetailItem } from 'constants/interface';
import styles from './styles.module.scss';
import { convertTimeToLocal } from 'utils/helper';
import { FormatTime } from 'constants/enum';
import useProfile from 'utils/hooks/useProfile';

interface IProps {
  item: IChatDetailItem;
}

export default function ChatDetailItem({ item }: IProps) {
  const { profile } = useProfile();

  return (
    <div
      className={classNames(styles.chatDetailItem, {
        [styles.sender]: profile?.id === item?.senderId,
      })}
    >
      <div className={styles.inner}>
        <p className={styles.content}>{item?.content}</p>
        <p>{convertTimeToLocal(item?.createdAt, FormatTime.TIME)}</p>
      </div>
    </div>
  );
}
