import { useEffect } from 'react';
import { MdMenu, MdSearch } from 'react-icons/md';
import { useQuery } from '@tanstack/react-query';
import { useNavigate, useParams } from 'react-router-dom';
import { Input } from 'antd';

import styles from './styles.module.scss';
import { QueryKey } from 'constants/enum';
import { getConversations } from 'api/chat';
import { IChatDetailItem, IConversationItem } from 'constants/interface';
import ConversationItem from '../ConversationItem';

interface IProps {
  receiveMes?: IChatDetailItem;
}

export default function ChatConversation({ receiveMes }: IProps) {
  const params = useParams();
  const navigate = useNavigate();

  const { data: conversations } = useQuery([QueryKey.CONVERSATIONS], () =>
    getConversations()
  );

  useEffect(() => {
    if (!params?.id && conversations) {
      const firstConversation = conversations?.[0];
      if (firstConversation) {
        navigate(`/chat/${firstConversation?.id}`);
      }
    }
  }, [conversations]);

  const handleSelectConversation = (item: IConversationItem) => {
    navigate(`/chat/${item?.id}`);
  };

  return (
    <div className={styles.conversation}>
      <div className={styles.header}>
        <div className={styles.menu}>
          <MdMenu style={{ fontSize: 24 }}></MdMenu>
        </div>
        <div className={styles.search}>
          <Input
            placeholder="Search"
            prefix={<MdSearch className={styles.searchIcon} />}
          />
        </div>
      </div>
      <div className={styles.list}>
        {conversations?.map((item: IConversationItem) => (
          <ConversationItem
            key={item?.id}
            item={item}
            idSelect={params?.id}
            onSelectConversation={handleSelectConversation}
          />
        ))}
      </div>
    </div>
  );
}
