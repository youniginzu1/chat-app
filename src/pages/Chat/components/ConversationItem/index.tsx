import classNames from 'classnames';
import { useContext } from 'react';

import { IChatContext, IConversationItem } from 'constants/interface';
import styles from './styles.module.scss';
import { convertTimeToLocal } from 'utils/helper';
import { FormatTime } from 'constants/enum';
import { ChatContext } from 'pages/Chat';

interface IProps {
  item: IConversationItem;
  idSelect?: string;
  onSelectConversation: (item: IConversationItem) => void;
}

export default function ConversationItem({
  item,
  idSelect,
  onSelectConversation,
}: IProps) {
  const { receiveMes } = useContext(ChatContext) as IChatContext;

  return (
    <div
      className={classNames(styles.conversationItem, {
        [styles.select]: idSelect && +idSelect && item?.id === +idSelect,
      })}
      onClick={() => {
        onSelectConversation(item);
      }}
    >
      <div className={styles.avatar}>
        <img src={item?.avatar} alt="" />
      </div>
      <div className={styles.user}>
        <p className={styles.userName}>{item?.name}</p>
        <div className={styles.lastMessage}>
          <span className="ellipsis-one-line">
            {receiveMes && receiveMes?.roomId === item?.id
              ? receiveMes?.content
              : item?.lastMessage?.content}
          </span>
          <span className={styles.time}>
            {convertTimeToLocal(
              receiveMes && receiveMes?.roomId === item?.id
                ? receiveMes?.createdAt
                : item?.lastMessage?.createdAt,
              FormatTime.TIME
            )}
          </span>
        </div>
      </div>
    </div>
  );
}
