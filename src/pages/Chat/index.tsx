import { useEffect, useState, createContext } from 'react';

import { IChatContext, IChatDetailItem } from 'constants/interface';
import ChatConversation from './components/ChatConversation';
import ChatFrame from './components/ChatFrame';
import styles from './styles.module.scss';
import socket from 'utils/helper/socket';

export const ChatContext = createContext<IChatContext | null>(null);

export default function Chat() {
  const [receiveMes, setReceiveMes] = useState<IChatDetailItem>();

  useEffect(() => {
    const onMessage = (message: IChatDetailItem) => {
      const chatHistory = document.querySelector('#chatHistory');
      if (Number(chatHistory?.scrollTop) !== 0) {
        chatHistory?.scrollTo(0, 0);
      }
      setReceiveMes(message);
    };
    socket.on('message', onMessage);
    return () => {
      socket.off('message', onMessage);
    };
  }, []);

  return (
    <ChatContext.Provider value={{ receiveMes: receiveMes }}>
      <div className={styles.chat}>
        <ChatConversation />
        <ChatFrame />
      </div>
    </ChatContext.Provider>
  );
}
