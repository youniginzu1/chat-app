import { useTranslation } from 'react-i18next';

import styles from './styles.module.scss';
import TaskFilter from './components/TaskFilter';
import useFilter from 'utils/hooks/useFilter';
import PaginationCustom from 'components/Table/PaginationCustom';
import TaskTable from './components/TaskTable';
import { defaultFilterExample, taskData } from 'constants/defaultValue';

export default function Task() {
  const [t] = useTranslation();

  const { filter, handleSearch, handlePageChange } = useFilter();

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.title}>{t('nav.task')}</div>
      </div>
      <div className={styles.content}>
        <div className={styles.filterContainer}>
          <div className={styles.filter}>
            <TaskFilter filter={filter} handleSearch={handleSearch} />
          </div>
          <div className={styles.actions}></div>
        </div>
        <div className={styles.table}>
          <TaskTable filter={filter} data={taskData || []} loading={false} />
        </div>
        <div className={styles.pagination}>
          <PaginationCustom
            pageIndex={filter.pageIndex || defaultFilterExample.pageIndex}
            pageSize={filter.pageSize || defaultFilterExample.pageSize}
            total={taskData.length || 0}
            onChange={handlePageChange}
          />
        </div>
      </div>
    </div>
  );
}
