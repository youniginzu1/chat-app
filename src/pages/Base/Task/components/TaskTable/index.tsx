import { useTranslation } from 'react-i18next';
import { ColumnsType } from 'antd/lib/table';
import { Table } from 'antd';

import { IFilter, ITaskRow } from 'constants/interface';
import { convertTimeToLocal, getIndexTable } from 'utils/helper';
import { Delete, Detail, Edit } from 'components/Table/TableAction';

interface IProps {
  filter: IFilter;
  data: ITaskRow[];
  loading: boolean;
}

export default function TaskTable({ filter, data, loading }: IProps) {
  const [t] = useTranslation();

  const columns: ColumnsType<ITaskRow> = [
    {
      title: t('table.index'),
      dataIndex: 'index',
      render: (value: any, record: ITaskRow, index: number) => (
        <span>
          {getIndexTable(filter.pageIndex || 1, filter.pageSize || 10, index)}
        </span>
      ),
      width: '5%',
    },
    {
      title: t('table.taskName'),
      dataIndex: 'name',
      render: (value, record, index) => <span>{record?.name}</span>,
    },
    {
      title: t('table.amount'),
      dataIndex: 'amount',
      render: (value, record, index) => <span>{record?.amount}</span>,
    },
    {
      title: t('table.createdAt'),
      dataIndex: 'createdAt',
      render: (value, record, index) => (
        <span>{convertTimeToLocal(record?.createdAt)}</span>
      ),
      width: '10%',
    },
    {
      title: t('table.action'),
      render: (value, record, index) => {
        return (
          <>
            <Detail onClick={() => {}} className="mr-10" />
            <Edit onClick={() => {}} className="mr-10" />
            <Delete onClick={() => {}} />
          </>
        );
      },
      width: '15%',
    },
  ];

  return (
    <>
      <Table
        rowKey={(obj) => obj.id}
        dataSource={data}
        columns={columns}
        pagination={false}
        loading={loading}
        bordered
        className="table-custom"
      />
    </>
  );
}
