import { useTranslation } from 'react-i18next';
import { Input } from 'antd';

import { IFilter } from 'constants/interface';

interface IProps {
  filter: IFilter;
  handleSearch: any;
}

export default function TaskFilter({ filter, handleSearch }: IProps) {
  const [t] = useTranslation();

  return (
    <>
      <Input.Search
        placeholder={t('placeholder.keyword')}
        onChange={handleSearch?.keywordSearch}
        className="input-search-custom"
      />
    </>
  );
}
