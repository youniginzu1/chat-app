import userIcon from './images/user-icon.svg';
import bg from './images/bg.jpg';
import girl from './images/girl.png';
import leaf01 from './images/leaf_01.png';
import leaf02 from './images/leaf_02.png';
import leaf03 from './images/leaf_03.png';
import leaf04 from './images/leaf_04.png';
import trees from './images/trees.png';

const images = {
  userIcon,
  bg,
  girl,
  leaf01,
  leaf02,
  leaf03,
  leaf04,
  trees,
};
export default images;
