export enum TokenType {
  TOKEN = 'accessToken',
  REFRESH_TOKEN = 'refreshToken',
}

export enum ErrorCode {
  UNAUTHORIZED = 'Unauthorized',
}

export enum ResponseStatus {
  NOT_FOUND = 404,
  FORBIDDEN = 403,
}

export enum DebounceTime {
  DEFAULT = 500,
}

export enum FormatTime {
  DATE_TIME = 'DD/MM/YYYY HH:mm',
  DATE = 'DD/MM/YYYY',
  DATE_REVERSE = 'YYYY/MM/DD',
  TIME = 'HH:mm',
}

export enum LanguageType {
  JA = 'ja',
  EN = 'en',
  KEY = 'cimode',
  VI = 'vi',
}

export enum LocalStorageCode {
  I18 = 'i18nextLng',
}

export enum QueryKey {
  PROFILE = 'profile',
  SHOW_SIDE_NAV = 'showSideNav',
  CONVERSATIONS = 'conversations',
  CHAT_HISTORIES = 'chatHistories',
  CONVERSATION_DETAIL = 'conversationDetail',
}

export enum AppEnv {
  PROD = 'prod',
}
