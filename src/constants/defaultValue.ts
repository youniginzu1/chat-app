import { ITaskRow } from './interface';

export const MIN_LENGTH_PASSWORD = 6;

export const MAX_LENGTH_PASSWORD = 25;

export const MAX_LENGTH_USERNAME = 50;

export const defaultFilterExample = {
  pageIndex: 1,
  pageSize: 10,
};

export const taskData: ITaskRow[] = [
  {
    id: 1,
    name: 'task1',
    amount: 1000,
    createdAt: '2023-03-12T23:50:33.526Z',
  },
  {
    id: 2,
    name: 'task2',
    amount: 2000,
    createdAt: '2023-03-12T23:50:33.526Z',
  },
  {
    id: 3,
    name: 'task3',
    amount: 3000,
    createdAt: '2023-03-12T23:50:33.526Z',
  },
];
