export interface IFilter {
  pageIndex?: number;
  pageSize?: number;
  keyword?: string;
}

export interface ITaskRow {
  id: number;
  name: string;
  amount: number;
  createdAt: string;
}

export interface IProfile {
  id: number;
  avatar: string;
  createdAt: string;
  email: string;
  fullName: string;
  mobile: string;
  status: number;
  updatedAt: string;
  username: string;
}

export interface IChatDetailItem {
  _id: number;
  roomId: number;
  content: string;
  senderId: number;
  receiverId: number;
  createdAt: string;
}

export interface IConversationItem {
  id: number;
  avatar?: string;
  name?: string;
  content: string;
  newMessage?: number;
  createdAt: string;
  lastMessage?: IChatDetailItem;
}

export interface IChatContext {
  receiveMes?: IChatDetailItem;
}
